#!/usr/bin/env python3

import json
import requests
import config as cfg
import pandas as pd

def config_checker():
    """Checks to ensure the config.py file is present and, if so,
    assigns the GITLAB_TOKEN as the PRIVATE-TOKEN variable within
    the headers dictionary for API calls.
    
    input:
        None
        
    output:
        None
    """
    
    # check to ensure length of GITLAB_TOKEN in config.py is more than 0
    if len(cfg.secrets['GITLAB_TOKEN']) > 0:
        # set the GITLAB_TOKEN as the PRIVATE-TOKEN header
        headers = {
            "PRIVATE-TOKEN": cfg.secrets['GITLAB_TOKEN']
        }
    else:
        # if length of GITLAB_TOKEN is less than 0, flag this to the user
        print("Please add a config.py file with your GitLab API secret included.")
        
    return headers


def all_group_grabber():
    """Gets all groups and subgroups in GitLab organization
    
    input:
        None
    
    output:
        id_list (list): List of all GitLab group and subgroup IDs
    """
    
    # instantiate an empty list to hold GitLab group IDs
    id_list = []
    
    # call GitLab API to get group information
    response = requests.get(
        "https://gitlab.com/api/v4/groups/?top_level_only=false&per_page=100",
        headers = config_checker()
    )
    
    # iterate through group information and append IDs to id_list
    for n in range(0, len(response.json())):
        id_list.append(response.json()[n]['id'])
        
    return id_list


def get_group_projects(group_id):
    """Gets all projects under a given GitLab group
    
    input:
        group_id (string): GitLab group ID
    
    output:
        project_id_list (list): list of GitLab project IDs underneath the given group
    """

    # add group_id to the API endpoint URL
    group_projects_id = "https://gitlab.com/api/v4/groups/" + str(group_id) + "/projects/?per_page=100"
    
    # set the headers variable
    headers = config_checker()
    
    # call the GitLab API to get all project IDs associated with the group_id
    response = requests.get(
        group_projects_id,
        headers = headers
    )
    
    # instantiate an empty list to hold project IDs
    project_id_list = []
    
    # iterate through project ID list
    for n in range(0, len(response.json())):
        # append project IDs to the empty list
        project_id_list.append(str(response.json()[n]['id']))
        
    return project_id_list


def push_rule_grabber(project_id_list):
    """Get push rules from all projects in project_id list
    
    input:
        project_id_list (list): list of GitLab project IDs
        
    output:
        df (pandas.core.frame.DataFrame): Dataframe with push rules data
    """
    id_list = []
    project_ids_list = []
    branch_name_regex_list = []
    
    for n in range(0, len(project_id_list)):
        response = requests.get(
            "https://gitlab.com/api/v4/projects/" + str(project_id_list[n]) + "/push_rule?per_page=100",
            headers = config_checker()
        )
        
        id_list.append(int(response.json()['id']))
        project_ids_list.append(int(response.json()['project_id']))
        branch_name_regex_list.append(response.json()['branch_name_regex'])
    
    
    df = pd.DataFrame(
        {
            'id': id_list,
            'project_id': project_ids_list,
            'branch_name_regex': branch_name_regex_list
        }
    )
    
    return df


def main():
    """Executes all functions
    
    input:
        None
        
    output:
        dataframe (pandas.core.frame.DataFrame): dataframe of push rules
    """
    
    id_list = all_group_grabber()
    project_id_list = []
    for n in id_list:
        x = get_group_projects(n)
        if len(x) == 1:
            project_id_list.append(x[0])
        elif len(x) > 1:
            for y in range(0, len(x)):
                project_id_list.append(x[y])
        else:
            pass
    dataframe = push_rule_grabber(project_id_list)
        
    dataframe.to_csv(
        'push_rules.csv',
        index = False
    )
    print(f"Pulled the push rules for {dataframe.shape[0]} projects.")
    
if __name__ == '__main__':
    main()