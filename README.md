# Allowing Snyk fix merge requests in GitLab
This repository helps Snyk users with GitLab Ultimate's push rules setting enabled and branch name validation added to open fix merge requests.

## Problem Statement
One of Snyk's most powerful features is the ability to create a fix merge request directly from the Snyk UI to allow users to easily remove vulnerabilities brought in by their open source dependencies.

GitLab Ultimate's push rules offer centralized control over certain repository settings, including branch name validation.

Here's an example of a branch name validation push rule (these are defined using a regex pattern):
![](img/push_rules_view.gif)

When Snyk users try to open a fix merge reques from the Snyk UI for a repository with branch name validation turned on, the following happens:
![](img/failed_fix_pr.gif)

To match a typical GitLab organization structure that can contain up to 20 subgroups within each of numerous groups, all with projects contained at the group and subgroup level, this repository has been tested with the following organization structure:
![](img/org_structure.png)

## Solution
Use the GitLab API to programmatically modify existing branch name validation rules to include the standard Snyk fix merge request branch name (`snyk-fix-123456789101112`)
![](img/successful_fix_pr.gif)

## Assumptions
This repository makes a few assumptions:
- You are a GitLab Ultimate user with the push rules branch name validation in use
- You have a GitLab personal access token with the "api" scope
- You have the necessary permissions to execute GET and PUT permissions via the GitLab API
- You are a Snyk user and you monitor your open source dependencies using Snyk

## Requirements
- Python 3
- GitLab API token saved within a `config.py` file. Here's an example:
    ```python
    #secret.py
    secrets = {
    "GITLAB_TOKEN": "glpat-123abcgitlabkey"
    }
    ```

## Getting started
- Install the required dependencies by executing `pip install requirements.txt`
- Execute the following command and follow the instructions: `python3 main.py`

## Limitations
Currently, the code in this repository only extracts branch name validation rules and saves them as a csv file.

The ability to change the branch name validation rules will be added soon.